import java.sql.*;
import java.util.*;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    //  Database credentials
    static final String USER = "huy";
    static final String PASS = "safe_pass_1337";

    public static void main(String[] args) {

        // better to clean tables before running that

        setup_test_employees(); // add 5 generic employees

        //add regular salaries, should work ok if there are no any data in the table and setup_test_employees called before
        LinkedList<Integer> ids = new LinkedList<>(Arrays.asList(1337, 1338, 1339));
        LinkedList<Integer> employee_ids = new LinkedList<>(Arrays.asList(228,229,230));
        LinkedList<Integer> amounts = new LinkedList<>(Arrays.asList(123,456,789));
        add_salaries(ids, employee_ids, amounts);

        // add additional entry as first to the transaction data
        //
        // here we try to add the list again but with new data at first,
        // than it should not work because we have some data with the same ids from previous batch

        ids.addFirst(1340);  // new id
        employee_ids.addFirst(231); // new user
        amounts.addFirst(12312);

        add_salaries(ids, employee_ids, amounts);

        System.out.println("Completed");

    }


    public static void setup_test_employees()
    {
        // omg those try catches are painfull
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);

            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            add_employee(228, 99, "'Anatoly'", "'Herkov'", conn, stmt);
            add_employee(229, 98, "'Pupa'", "' Lupin'", conn, stmt);
            add_employee(230, 97, "'Lupa'", "' Pupin'", conn, stmt);
            add_employee(231, 96, "'Matye'", "'Bal'", conn, stmt);
            add_employee(232, 95, "'Yasha'", "'Lava'", conn, stmt);

            stmt.close();
            conn.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }//end try

        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
                se2.printStackTrace();
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
    }

    public static void add_employee(int id, int age, String name, String surname, Connection conn, Statement stmt) {
        try{
            String SQL = "INSERT INTO Employees " + "VALUES (" + id + ", " + age + ","  + name +"," + surname + ")";
            stmt.executeUpdate(SQL);
            conn.commit();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }//end try
        }
    }

    public static void add_salaries(LinkedList<Integer> ids, LinkedList<Integer> employee_ids,  LinkedList<Integer> amount)
    {
        // omg those try catches are painfull
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);

            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            add_salaries( ids, employee_ids, amount, conn, stmt);

            stmt.close();
            conn.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }//end try

        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
                se2.printStackTrace();
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
    }

    public static void add_salaries(
            LinkedList<Integer> ids,
            LinkedList<Integer> employee_ids,
            LinkedList<Integer> amount,
            Connection conn,
            Statement stmt) {

        try{
            // Cache current employee ids
            String sql = "SELECT id FROM Employees";
            ResultSet rs = stmt.executeQuery(sql);
            HashSet<Integer> curr_employee_ids = new HashSet<Integer>();
            rs.beforeFirst();
            while(rs.next()){
                int emp_id = rs.getInt("id");
                curr_employee_ids.add(emp_id);
            }
            rs.close();

            // Cache current salaries ids
            sql = "SELECT id FROM Salary";
            rs = stmt.executeQuery(sql);
            HashSet<Integer> curr_salaries_ids = new HashSet<Integer>();
            rs.beforeFirst();
            while(rs.next()){
                int emp_id = rs.getInt("id");
                curr_salaries_ids.add(emp_id);
            }
            rs.close();

            // Cache current users with salaries
            sql = "SELECT employee_id FROM Salary";
            rs = stmt.executeQuery(sql);
            HashSet<Integer> curr_salaried_users_ids = new HashSet<Integer>();
            rs.beforeFirst();
            while(rs.next()){
                int emp_id = rs.getInt("employee_id");
                curr_salaried_users_ids.add(emp_id);
            }
            rs.close();



            for (int i = 0; i < Math.min(amount.size(), Math.min(ids.size(), employee_ids.size())); i++) {
                if (!curr_employee_ids.contains(employee_ids.get(i))) {
                    throw new SQLException("Wrong employee_id, employee with such id non exists: " + employee_ids.get(i));
                }else if(curr_salaries_ids.contains((ids.get(i))))
                {
                    throw new SQLException("Wrong id, already exist: " + ids.get(i));
                }else if(curr_salaried_users_ids.contains(employee_ids.get(i)))
                {
                    throw new SQLException("Wrong employee_id, already has salary: " + employee_ids.get(i));
                }
                else {
                    String SQL = "INSERT INTO Salary " +
                            "VALUES (" + ids.get(i) + "," +
                            employee_ids.get(i) + "," +
                            amount.get(i) + ")";
                    stmt.executeUpdate(SQL);
                }

                // update cached data
                curr_salaried_users_ids.add(employee_ids.get(i));
                curr_salaries_ids.add(ids.get(i));
            }


            System.out.println("Commiting data here....");
            conn.commit();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }//end try
        }

    }

}
